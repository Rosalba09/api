//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3m78019/collections/mivimientos?apiKey=NrrUArvkgDOE5Ew-tChgm-bpxEqbb7Cc";

var clienteMLab=requestjson.createClient(urlMovimientos);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/clientes/:idcliente', function (req, res){
  res.send('Aqui tiene el cliente numero:'+req.params.idcliente);
});

app.get('/mivimientos', function(req, res) {
var clienteMLab=requestjson.createClient(urlMovimientos);
   clienteMLab.get('',  function(err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      res.send(body);
    }
  });
});

//app.post('/', function (req, res){
  //res.send('Su petición ha sido recibida cambiada')
//});

app.post('/mivimientos', function(req, res) {
 clienteMLab.post('', req.body, function(err, resM, body) {
   if (err) {
     console.log(err);
   } else {
     res.send(body);
 }
});
});

app.put('/', function (req, res){
  res.send('Su petición ha sido enviada');
});

app.delete('/', function (req, res){
  res.send('Se ha borrado');
});
